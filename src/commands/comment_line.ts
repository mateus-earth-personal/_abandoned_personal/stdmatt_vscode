//----------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                       | |    | |               | | | |                     //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//                                                                            //
//  File      : comment_line.ts                                               //
//  Project   : stdmatt-vscode                                                //
//  Date      : 04 Jan, 22                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2022                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Import                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import * as vscode from "vscode";
//------------------------------------------------------------------------------
import { VS_Utils }           from "../vs_utils";
import { Ruler_Info }         from "../vs_utils";
import { Comment_Info }       from "../vs_utils";
import { Text_Process_Utils } from "../text_process_utils";

//------------------------------------------------------------------------------
const _comment_line = Text_Process_Utils.comment_line;


//----------------------------------------------------------------------------//
// Comment Line                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Command_Comment_Line
{
    //--------------------------------------------------------------------------
    static line_separator(): void
    {
        const editor = VS_Utils.get_editor();
        if(!editor) {
            return;
        }

        const document = VS_Utils.get_current_document();
        if(!document) {
            return;
        }

        const comment  = VS_Utils.get_comment(document);
        const ruler    = VS_Utils.get_ruler();
        const position = editor.selection.active;

        const line = _comment_line(comment, ruler, "", position.character, false, false, "-");
        editor.edit(editor_builder => {
            editor_builder.insert(position, line);
        });
    }

    //--------------------------------------------------------------------------
    static block_separator()
    {
        const editor = VS_Utils.get_editor();
        if(!editor) {
            return;
        }

        const document = VS_Utils.get_current_document();
        if(!document) {
            return;
        }

        const comment  = VS_Utils.get_comment(document);
        const ruler    = VS_Utils.get_ruler();
        const pos_orig = editor.selection.active;
        const position = document.getWordRangeAtPosition(pos_orig)?.start ?? pos_orig;
        const src_line = document.lineAt(position.line).text;

        // @TODO(stdmatt): Check this case // /* OLA */
        const clean_line = src_line
            .replace(comment.start, "")
            .replace(comment.end,   "")
            .trim();

        const border = _comment_line(comment, ruler, "",               position.character, false, true,  "-");
        const middle = _comment_line(comment, ruler, " " + clean_line, position.character, false, true,  " ");
        const last   = _comment_line(comment, ruler, "",               position.character, false, false, "-");
        const indent = " ".repeat(position.character);

        const line   =
            border          + "\n" +
            indent + middle + "\n" +
            indent + border + "\n" +
            indent + last          ;

        editor.edit(editor_builder => {
            const range_to_replace = new vscode.Range(
                new vscode.Position(position.line, position.character),
                new vscode.Position(position.line, src_line.length)
            );
            editor_builder.replace(range_to_replace, line);
        });
    }
}
