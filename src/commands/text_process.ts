//----------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                       | |    | |               | | | |                     //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//                                                                            //
//  File      : text_process.ts                                               //
//  Project   : stdmatt-vscode                                                //
//  Date      : 04 Jan, 22                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2022                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Import                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import * as vscode from "vscode";
//------------------------------------------------------------------------------
import { VS_Utils }     from "../vs_utils";
import { Ruler_Info }   from "../vs_utils";
import { Comment_Info } from "../vs_utils";


//----------------------------------------------------------------------------//
// Text Process                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Command_Text_Process
{
    //--------------------------------------------------------------------------
    static fix_slashes(): void
    {
        const editor = VS_Utils.get_editor();
        if(!editor) {
            return;
        }

        const document = VS_Utils.get_current_document();
        if(!document) {
            return;
        }

        const position = editor.selection.active;
        const src_line = document.lineAt(position.line).text;

        let clean_line = src_line;
        while(true) {
            const new_line = clean_line.replace("\\", "/");
            if(new_line == clean_line) {
                break;
            }
            clean_line = new_line;
        }

        editor.edit(editor_builder => {
            const range_to_replace = new vscode.Range(
                new vscode.Position(position.line, 0),
                new vscode.Position(position.line, src_line.length)
            );

            editor_builder.replace(range_to_replace, clean_line);
        });
    }
}